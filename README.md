# CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Permissions
- Usage
- Maintainers

# INTRODUCTION

The *oEmbed provider endpoints* module provides a plugin system for exposing
Drupal entities via oEmbed endpoints.

> oEmbed is a format for allowing an embedded representation of a URL on third
  party sites. The simple API allows a website to display embedded content
  (such as photos or videos) when a user posts a link to that resource, without
  having to parse the resource directly.
  - oEmbed.com

A consumer application sends an HTTP `GET` request with two query parameters:
`format` and `url`. The provider application then responds with a payload that
represents the requested resource in the requested format.
See [oEmbed.com](https://oembed.com/) for more details.

For a full description of the module, visit the project page:
   https://www.drupal.org/project/oembed_provider_endpoints

To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/oembed_provider_endpoints

# REQUIREMENTS

This module has the following requirements:

- Drupal 8.9.0+ or Drupal 9.0.0+

# INSTALLATION

- Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

This module must be installed with Composer.

# PERMISSIONS

Access to all functionality provided by this module is controlled with the
*Administer oEmbed provider endpoints* permission.

# USAGE

Two parts comprise a functioning oEmbed endpoint:

1. the oEmbed provider endpoint
2. the data provider plugin.

## oEmbed Provider Endpoint

Endpoints are managed at `/admin/config/media/oembed-provider-endpoints`.

The following settings comprise an endpoint's configuration:

- Label
  - The title of the endpoint
- Machine name
  - A unique identifier for the endpoint
- Path
  - The absolute URL path for the endpoint
- Data provider plugin
  - The data provider plugin that will be processing and responding to requests

Endpoints are stored as configuration entities.

## Data Provider Plugin

Data provider plugins process resource requests and respond with resource data.

The *oEmbed provider endpoints* module ships with the *oEmbed Provider Endpoints: Node Data Provider* module, which exposes nodes at oEmbed resources.

## Create a Data Provider Module

A data provider plugin is a Drupal plugin.

See `\Drupal\oembed_provider_endpoints\OembedDataProviderPluginBase`.
See `\Drupal\oembed_provider_endpoints\OembedDataProviderPluginInterface`.
See `\Drupal\oembed_provider_endpoints\Traits\PluginHelperTrait`.

A plugin's optional configuration is stored with its endpoint's configuration.

The *oEmbed Provider Endpoints: Node Data Provider* module can also be reviewed for example code.

# MAINTAINERS

Current maintainers:
 * Chris Burge - https://www.drupal.org/u/chris-burge

This project has been sponsored by:
 * [University of Nebraska-Lincoln, Digital Experience Group](https://dxg.unl.edu)
