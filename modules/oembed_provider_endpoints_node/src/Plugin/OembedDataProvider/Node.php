<?php

namespace Drupal\oembed_provider_endpoints_node\Plugin\OembedDataProvider;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\oembed_provider_endpoints\OembedDataProviderPluginBase;
use Drupal\oembed_provider_endpoints\OembedResourceResponse;
use Drupal\oembed_provider_endpoints\Traits\PluginHelperTrait;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use unl\OembedResource\OembedResource;

/**
 * Provides a node data provider plugin.
 *
 * @OembedDataProviderPlugin(
 *   id = "node",
 *   description = @Translation("Node"),
 * )
 */
class Node extends OembedDataProviderPluginBase implements ContainerFactoryPluginInterface {

  use PluginHelperTrait;
  use StringTranslationTrait;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityDisplayRepositoryInterface $entity_display_repository, RendererInterface $renderer, EntityTypeManagerInterface $entity_type_manager) {
    $this->configuration = $configuration;
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.bundle.info'),
      $container->get('entity_display.repository'),
      $container->get('renderer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElements() {
    $endpoint_id = $this->getEndpointIdFromRoute();
    $plugin_config = $this->getDataProviderPluginConfig($endpoint_id);

    $node_entity_info = $this->entityTypeBundleInfo->getBundleInfo('node');
    $content_type_options = [];
    foreach ($node_entity_info as $id => $info) {
      $content_type_options[$id] = $info['label'];
    }
    $form['content_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Types'),
      '#description' => $this->t('The content types that will be exposed via this endpoint.'),
      '#default_value' => $plugin_config['oembed_provider_endpoints_node:node']['content_types'] ?? [],
      '#options' => $content_type_options,
      '#multiple' => TRUE,
      '#required' => TRUE,
    ];

    $view_modes = $this->entityDisplayRepository->getAllViewModes()['node'];
    $view_mode_options = [];
    foreach ($view_modes as $id => $view_mode) {
      $view_mode_options[$id] = $view_mode['label'];
    }
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View Mode'),
      '#description' => $this->t('The view mode used to render content for this endpoint.'),
      '#default_value' => $plugin_config['oembed_provider_endpoints_node:node']['view_mode'] ?? [],
      '#options' => $view_mode_options,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigFromFormState(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    return [
      'content_types' => Checkboxes::getCheckedCheckboxes($values['content_types']),
      'view_mode' => $values['view_mode'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resource($endpoint_id, Url $url) {
    // Check route parameters for NID.
    $route_parameters = $url->getRouteParameters();
    if (empty($route_parameters['node'])) {
      return FALSE;
    }

    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    $storage = $this->entityTypeManager->getStorage('node');
    $node = $storage->load($route_parameters['node']);

    // Check that requested node exists.
    if (is_null($node)) {
      return FALSE;
    }

    // Check that requested node is an allowed bundle.
    if (!in_array($node->bundle(), $this->configuration['content_types'])) {
      return FALSE;
    }

    // Check if anonymous user has access to node.
    /** @var \Drupal\user\UserInterface */
    $anon_user = User::getAnonymousUser();
    if (!$node->access('view', $anon_user)) {
      return FALSE;
    }

    $view_mode = $this->configuration['view_mode'];
    $this->build = $view_builder->view($node, $view_mode);
    $output = $this->renderer->render($this->build);

    // Build OembedResource object.
    $resource = new OembedResource('rich');
    $resource->setTitle($node->getTitle());
    $resource->setHtml($output);
    $resource->setWidth(1);
    $resource->setHeight(1);

    // Build OembedResourceResponse object.
    $return = new OembedResourceResponse($resource);
    $return->setCacheableDependencies([$node]);

    return $return;
  }

}
