<?php

namespace Drupal\oembed_provider_endpoints;

use Drupal\Core\Cache\CacheableMetadata;
use unl\OembedResource\OembedResource;

/**
 * An object that represents an oEmbed Resource response.
 */
class OembedResourceResponse {

  /**
   * An OembedResource object.
   *
   * @var \unl\OembedResource\OembedResource
   */
  protected $resource;

  /**
   * An array of cacheable dependencies.
   *
   * @var array
   */
  protected $cacheableDependencies;

  /**
   * A CacheableMetadata object.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $cacheableMetadata;

  /**
   * Constructor.
   *
   * @param \unl\OembedResource\OembedResource $resource
   *   An OembedResource object.
   */
  public function __construct(OembedResource $resource) {
    $this->resource = $resource;
  }

  /**
   * Gets the resource property.
   *
   * @return \unl\OembedResource\OembedResource
   *   An OembedResource object.
   */
  public function getResource() {
    return $this->resource;
  }

  /**
   * Set the cacheableDependencies property.
   *
   * @param array $cacheable_dependencies
   *   An array of cacheable dependencies.
   *
   * @see \Drupal\Core\Cache\RefinableCacheableDependencyTrait::addCacheableDependency
   */
  public function setCacheableDependencies(array $cacheable_dependencies) {
    $this->cacheableDependencies = $cacheable_dependencies;
  }

  /**
   * Gets the cacheableDependencies property.
   *
   * @return array
   *   An array of cacheable dependencies.
   */
  public function getCacheableDependencies() {
    return $this->cacheableDependencies;
  }

  /**
   * Set the cacheableMetadata property.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheable_metadata
   *   A CacheableMetadata object.
   */
  public function setCacheableMetadata(CacheableMetadata $cacheable_metadata) {
    $this->cacheableMetadata = $cacheable_metadata;
  }

  /**
   * Gets the cacheableMetadata property.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   A CacheableMetadata object.
   */
  public function getCacheableMetadata() {
    return $this->cacheableMetadata;
  }

}
