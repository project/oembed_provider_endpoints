<?php

namespace Drupal\oembed_provider_endpoints;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * A base class oEmbed data provider plugins.
 *
 * @see \Drupal\oembed_provider_endpoints\Annotation\OembedDataProviderPlugin
 * @see \Drupal\oembed_provider_endpoints\OembedDataProviderPluginInterface
 */
abstract class OembedDataProviderPluginBase extends PluginBase implements OembedDataProviderPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Provides a form array for plugin configuration.
   *
   * @return array
   *   The plugin configuration form array.
   */
  public function formElements() {
    return [
      'no-configuration' => [
        '#markup' => $this->t('<p>This plugin has no configuration.</p>'),
      ],
    ];
  }

  /**
   * Validates form input.
   *
   * @param array $form
   *   Form elements array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Return plugin configuration from form.
   *
   * Values should be returned in an associative array. Modules implementing
   * a OembedDataProviderPlugin also need to provide a schema that matches
   * the data returned by this method.
   *
   * @param array $form
   *   Form elements array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   An array of plugin values to be stored with the endpoint's config.
   */
  public function getConfigFromFormState(array &$form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * Returns an OembedResourceResponse object to the controller.
   *
   * @param string $endpoint_id
   *   The ID (machine name) of an oembed provider endpoint.
   * @param \Drupal\Core\Url $url
   *   A URL object.
   *
   * @return mixed
   *   \Drupal\oembed_provider_endpoints\OembedResourceResponse if resource is
   *   found; FALSE if resource is not found. A FALSE response will result in
   *   a 404 HTTP response from the controller.
   */
  abstract public function resource($endpoint_id, Url $url);

}
