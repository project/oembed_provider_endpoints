<?php

namespace Drupal\oembed_provider_endpoints\Traits;

/**
 * Methods to help oEmbed Provider Endpoints plugins.
 */
trait PluginHelperTrait {

  /**
   * Gets the definitions of oEmbed data provider plugins.
   *
   * @return array
   *   Definitions of oEmbed data provider plugins.
   */
  protected function getDataProviderPluginDefinitions() {
    $data_providers = \Drupal::service('plugin.manager.oembed_data_provider');
    return $data_providers->getDefinitions();
  }

  /**
   * Gets the plugin ID from an provider endpoint id (machine name).
   *
   * @param string $endpoint_id
   *   The ID (machine name) of an oembed provider endpoint.
   *
   * @return string
   *   The ID of the oEmbed data provider plugin used by the given endpoint.
   */
  protected function getDataProviderPluginId($endpoint_id) {
    return \Drupal::config("oembed_provider_endpoints.provider_endpoint.$endpoint_id")->get('plugin');
  }

  /**
   * Gets the plugin config from an provider endpoint id (machine name).
   *
   * @param string $endpoint_id
   *   The ID (machine name) of an oembed provider endpoint.
   *
   * @return array
   *   The config for the oEmbed data provider plugin used by the
   *   given endpoint.
   */
  protected function getDataProviderPluginConfig($endpoint_id) {
    return \Drupal::config("oembed_provider_endpoints.provider_endpoint.$endpoint_id")->get('plugin_config');
  }

  /**
   * Gets the oEmbed provider endpoint id from the current route.
   *
   * @return string
   *   The oEmbed provider endpoint id (machine name).
   */
  protected function getEndpointIdFromRoute() {
    return \Drupal::routeMatch()->getParameter('oembed_provider_endpoint')->id();
  }

  /**
   * Get the plugin instance from the current route.
   *
   * @return \Drupal\oembed_provider_endpoints\OembedDataProviderPluginInterface
   *   An instance of the oEmbed data provider plugin.
   */
  protected function getDataProviderPluginFromRoute() {
    $endpoint_id = $this->getEndpointIdFromRoute();
    $plugin_id = $this->getDataProviderPluginId($endpoint_id);

    return $this->getDataProviderPluginInstance($plugin_id, $endpoint_id);
  }

  /**
   * Get the plugin instance given a plugin ID and an endpoint ID.
   *
   * @param string $plugin_id
   *   The ID of the oEmbed data provider plugin.
   * @param string $endpoint_id
   *   The ID (machine name) of an oembed provider endpoint.
   *
   * @return \Drupal\oembed_provider_endpoints\OembedDataProviderPluginInterface
   *   An instance of the oEmbed data provider plugin.
   */
  protected function getDataProviderPluginInstance($plugin_id, $endpoint_id) {
    $plugin_manager = \Drupal::service('plugin.manager.oembed_data_provider');
    $plugin_config = \Drupal::config("oembed_provider_endpoints.provider_endpoint.$endpoint_id")->get('plugin_config');
    $plugin_config = !is_null($plugin_config) ? array_shift($plugin_config) : [];

    return $plugin_manager->createInstance($plugin_id, $plugin_config);
  }

}
