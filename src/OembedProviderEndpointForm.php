<?php

namespace Drupal\oembed_provider_endpoints;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\PreloadableRouteProviderInterface;
use Drupal\oembed_provider_endpoints\Traits\PluginHelperTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the oEmbed provider endpoint add and edit forms.
 */
class OembedProviderEndpointForm extends EntityForm {

  use PluginHelperTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route provider service.
   *
   * @var \Drupal\Core\Routing\PreloadableRouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Constructs an OembedProviderEndpointForm object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, PreloadableRouteProviderInterface $route_provider) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the oEmbed provider endpoint'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#default_value' => $this->entity->get('path'),
      '#size' => 60,
      '#required' => TRUE,
    ];

    $plugin_definitions = $this->getDataProviderPluginDefinitions();
    $plugin_options = [];
    foreach ($plugin_definitions as $plugin_definition) {
      $plugin_options[$plugin_definition['id']] = $plugin_definition['description'];
    }
    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Data provider plugin'),
      '#options' => $plugin_options,
      '#default_value' => $this->entity->get('plugin'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $path_valid = $this->validatePath($values['path']);
    if ($path_valid !== TRUE) {
      $form_state
        ->setErrorByName('path', $path_valid);
    }
    if (!$this->isPathAvailable($values['path'])) {
      $form_state
        ->setErrorByName('path', 'The requested path is unavailable for use.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label oEmbed provider endpoint was created.', [
        '%label' => $this->entity->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label oEmbed provider endpoint was updated.', [
        '%label' => $this->entity->label(),
      ]));
    }

    $form_state->setRedirect('entity.oembed_provider_endpoint.collection');
  }

  /**
   * Helper function to check whether a config entity exists by a given ID.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('oembed_provider_endpoint')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

  /**
   * Validates the path for the endpoint.
   *
   * Adapted from Drupal\views\Plugin\views\display\PathPluginBase.
   *
   * @param string $path
   *   The path to validate.
   *
   * @return mixed
   *   TRUE if validation is successful. An error string otherwise.
   */
  protected function validatePath($path) {
    if (strpos($path, '/') !== 0) {
      return 'Path must start with a forward slash.';
    }

    $parsed_url = UrlHelper::parse($path);
    if (empty($parsed_url['path'])) {
      return 'Path is empty.';
    }

    if (!empty($parsed_url['query'])) {
      return $this->t('No query allowed.');
    }

    if (!empty($parsed_url['fragment'])) {
      return $this->t('No fragment allowed.');
    }

    if (!parse_url('internal:/' . $path)) {
      return $this->t('Invalid path. Valid characters are alphanumerics as well as "-", ".", "_" and "~".');
    }

    return TRUE;
  }

  /**
   * Determines if a path is available.
   *
   * @param string $path
   *   The path.
   *
   * @return bool
   *   TRUE if route can be used (i.e. route is not registered or is
   *     registered by this config entity.)
   *   FALSE if route cannot be used (i.e. route is registered by anything
   *     other than this config entity)
   */
  protected function isPathAvailable($path) {
    $routes = $this->routeProvider->getRoutesByPattern($path)->all();

    if (empty($routes)) {
      // No routes found for path.
      return TRUE;
    }
    else {
      $entity_id = $this->getEntity()->id();
      $route_name = array_key_first($routes);
      // Check if path is registered to current endpoint config entity.
      preg_match('/oembed_provider_endpoints.provider_endpoint.(.*)/', $route_name, $matches);
      if (isset($matches[1]) && $matches[1] == $entity_id) {
        // Path is registered to current endpoint config entity.
        return TRUE;
      }
      // Path is registered by something else.
      return FALSE;
    }
  }

}
