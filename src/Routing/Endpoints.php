<?php

namespace Drupal\oembed_provider_endpoints\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * A routing class for dynamically registered oEmbed endpoint routes.
 */
class Endpoints {

  /**
   * Dynamically generate the routes for oEmbed endpoints.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   A route collection.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function routes() {
    $collection = new RouteCollection();

    // Loop through endpoint config entities and dynamically register
    // their routes.
    $endpoints = \Drupal::service('entity_type.manager')
      ->getStorage('oembed_provider_endpoint')
      ->loadMultiple();
    foreach ($endpoints as $endpoint) {
      $endpoint_id = $endpoint->id();
      $route = new Route(
        $endpoint->get('path'),
        [
          '_title' => $endpoint->get('label'),
          '_controller' => '\Drupal\oembed_provider_endpoints\Controller\Endpoint::response',
        ],
        [
          '_access' => 'TRUE',
        ],
        [],
        NULL,
        [],
        [
          'GET',
        ],
      );
      $collection->add("oembed_provider_endpoints.provider_endpoint.$endpoint_id", $route);
    }

    return $collection;
  }

}
