<?php

namespace Drupal\oembed_provider_endpoints\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\oembed_provider_endpoints\Traits\PluginHelperTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The data provider plugin configuration form.
 */
class OembedProviderEndpointPluginConfigurationForm extends EntityForm {

  use PluginHelperTrait;

  /**
   * The oEmbed data provider plugin.
   *
   * @var \Drupal\oembed_provider_endpoints\OembedDataProviderPluginInterface
   */
  protected $plugin;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->plugin = $this->getDataProviderPluginFromRoute();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oembed_provider_endpoints_plugin_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get form elements from data provider plugin.
    $form = $this->plugin->formElements();

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugin_definition = $this->plugin->getPluginDefinition();

    $plugin_config = [
      $plugin_definition['provider'] . ':' . $plugin_definition['id'] => $this->plugin->getConfigFromFormState($form, $form_state),
    ];

    $endpoint_id = $this->getEndpointIdFromRoute();
    $this->configFactory->getEditable("oembed_provider_endpoints.provider_endpoint.$endpoint_id")
      ->set('plugin_config', $plugin_config)
      ->save();
  }

}
