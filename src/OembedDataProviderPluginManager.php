<?php

namespace Drupal\oembed_provider_endpoints;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\oembed_provider_endpoints\Annotation\OembedDataProviderPlugin;

/**
 * A plugin manager for oEmbed provider data provider plugins.
 */
class OembedDataProviderPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/OembedDataProvider';
    $plugin_interface = OembedDataProviderPluginInterface::class;
    $plugin_definition_annotation_name = OembedDataProviderPlugin::class;
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
    $this->alterInfo('oembed_data_provider_info');
    $this->setCacheBackend($cache_backend, 'oembed_data_provider_info');
  }

}
