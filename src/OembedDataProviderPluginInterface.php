<?php

namespace Drupal\oembed_provider_endpoints;

/**
 * An interface for oEmbed data provider plugins.
 */
interface OembedDataProviderPluginInterface {

  /**
   * Provides a description of the plugin.
   *
   * @return string
   *   A string description of the plugin.
   */
  public function description();

}
