<?php

namespace Drupal\oembed_provider_endpoints\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the oEmbed provider endpoint entity.
 *
 * @ConfigEntityType(
 *   id = "oembed_provider_endpoint",
 *   label = @Translation("oEmbed provider endpoint"),
 *   label_collection = @Translation("oEmbed Provider Endpoints"),
 *   label_singular = @Translation("oembed provider endpoint"),
 *   label_plural = @Translation("oembed provider endpoints"),
 *   label_count = @PluralTranslation(
 *     singular = "@count oembed provider endpoint",
 *     plural = "@count oembed provider endpoints",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\oembed_provider_endpoints\OembedProviderEndpointListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\oembed_provider_endpoints\OembedProviderEndpointForm",
 *       "add" = "Drupal\oembed_provider_endpoints\OembedProviderEndpointForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "configure" = "Drupal\oembed_provider_endpoints\Form\OembedProviderEndpointPluginConfigurationForm"
 *     }
 *   },
 *   admin_permission = "administer oembed provider endpoints",
 *   config_prefix = "provider_endpoint",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "path",
 *     "plugin",
 *     "plugin_config"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/media/oembed-provider-endpoints/{oembed_provider_endpoint}/edit",
 *     "delete-form" = "/admin/config/media/oembed-provider-endpoints/{oembed_provider_endpoint}/delete",
 *     "configure-plugin-form" = "/admin/config/media/oembed-provider-endpoints/{oembed_provider_endpoint}/configure",
 *     "collection" = "/admin/config/media/oembed-provider-endpoints",
 *   }
 * )
 */
class OembedProviderEndpoint extends ConfigEntityBase {

  /**
   * The oEmbed provider endpoint ID (machine name).
   *
   * @var string
   */
  protected $id;

  /**
   * The oEmbed provider endpoint label.
   *
   * @var string
   */
  protected $label;

  /**
   * The oEmbed endpoint's path.
   *
   * @var string
   */
  protected $path;

  /**
   * The name of the data provider plugin.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The configuration of the data provider plugin.
   *
   * @var array
   */
  protected $plugin_config;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Rebuild route data to register changes to dynamically generated routes.
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Rebuild route data to register changes to dynamically generated routes.
    \Drupal::service('router.builder')->rebuild();
  }

}
