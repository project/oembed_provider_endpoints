<?php

namespace Drupal\oembed_provider_endpoints\Controller;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Drupal\oembed_provider_endpoints\Traits\PluginHelperTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handles responses for oEmbed provider endpoints.
 */
class Endpoint extends ControllerBase {

  use PluginHelperTrait;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The path validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The oEmbed data provider plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $oembedDataProviderPluginManager;

  /**
   * Class constructor.
   */
  public function __construct(RequestStack $request_stack, ResettableStackedRouteMatchInterface $current_route_match, PathValidatorInterface $path_validator, RendererInterface $renderer, PluginManagerInterface $plugin_manager) {
    $this->requestStack = $request_stack;
    $this->currentRouteMatch = $current_route_match;
    $this->pathValidator = $path_validator;
    $this->renderer = $renderer;
    $this->oembedDataProviderPluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('path.validator'),
      $container->get('renderer'),
      $container->get('plugin.manager.oembed_data_provider')
    );
  }

  /**
   * Returns a response for the request.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   A cacheable response object.
   */
  public function response() {
    /** @var \Symfony\Component\HttpFoundation\Request */
    $request = $this->requestStack->getCurrentRequest();

    // Get query parameters and validate.
    /** @var \Symfony\Component\HttpFoundation\ParameterBag */
    $query_parameters = $request->query;

    // Validate the 'format' query parameter.
    $format = $query_parameters->get('format');
    if (!in_array($format, ['json', 'xml'])) {
      $response = new CacheableResponse('Valid format is required.', Response::HTTP_BAD_REQUEST);
      // Respect format and url query parameters for caching.
      $response->getCacheableMetadata()->addCacheContexts([
        'url.query_args:format',
        'url.query_args:url',
      ]);
      // Add cache tag for route, so any changes to the endpoint configuration
      // will invalidate all previously cached responses.
      $response->getCacheableMetadata()->addCacheTags([
        'oembed_provider_endpoints.provider_endpoint.' . $this->endpoint_id,
      ]);
      return $response;
    }

    // Validate thr 'url' query parameter.
    $this->url = $query_parameters->get('url');
    if (!$this->pathValidator->isValid($this->url)) {
      $response = new CacheableResponse('Valid URL is required.', Response::HTTP_BAD_REQUEST);
      $response->getCacheableMetadata()->addCacheContexts([
        'url.query_args:format',
        'url.query_args:url',
      ]);
      $response->getCacheableMetadata()->addCacheTags([
        'oembed_provider_endpoints.provider_endpoint.' . $this->endpoint_id,
      ]);
      return $response;
    }

    // Get endpoint id from route and load data provider plugin.
    /** @var \Drupal\Core\Routing\RouteMatch */
    $route_name = $this->currentRouteMatch->getRouteMatchFromRequest($request)->getRouteName();
    $route_name_parts = explode('.', $route_name);
    $this->endpoint_id = end($route_name_parts);
    $plugin_id = $this->getDataProviderPluginId($this->endpoint_id);
    /** @var \Drupal\oembed_provider_endpoints\OembedDataProviderPluginInterface */
    $this->plugin = $this->getDataProviderPluginInstance($plugin_id, $this->endpoint_id);

    // Workaround for early rendering.
    // https://www.lullabot.com/articles/early-rendering-a-lesson-in-debugging-drupal-8
    $context = new RenderContext();
    /** @var \Drupal\Core\Cache\CacheableDependencyInterface */
    $resource_response = $this->renderer->executeInRenderContext($context, function () {
      $url = $this->getUrlOjbFromUrlString($this->url);
      if (!$url) {
        return FALSE;
      }
      return $this->plugin->resource($this->endpoint_id, $url);
    });

    // Is this necessary? Plugins are responsible for passing up their own
    // cache dependencies.
    // Handle any bubbled cacheability metadata.
    if (!$context->isEmpty()) {
      $bubbleable_metadata = $context->pop();
      BubbleableMetadata::createFromObject($resource_response)
        ->merge($bubbleable_metadata);
    }

    // If the resource response is FALSE, then the resource was not found.
    if (!$resource_response) {
      $response = new CacheableResponse('Resource not found.', Response::HTTP_NOT_FOUND);
      $response->getCacheableMetadata()->addCacheContexts([
        'url.query_args:format',
        'url.query_args:url',
      ]);
      $response->getCacheableMetadata()->addCacheTags([
        'oembed_provider_endpoints.provider_endpoint.' . $this->endpoint_id,
      ]);
      return $response;
    }

    // At this point, there should be a valid resource to return.
    // Prepare response per requested format.
    if ($format == 'json') {
      $payload = $resource_response->getResource()->generate('json');
      $content_type = 'application/json';
    }
    elseif ($format = 'xml') {
      $payload = $resource_response->getResource()->generate('xml');
      $content_type = 'text/xml';
    }

    $response = new CacheableResponse($payload, Response::HTTP_OK, ['Content-Type' => $content_type]);
    $response->getCacheableMetadata()->addCacheContexts([
      'url.query_args:format',
      'url.query_args:url',
    ]);
    $response->getCacheableMetadata()->addCacheTags([
      'oembed_provider_endpoints.provider_endpoint.' . $this->endpoint_id,
    ]);
    // Add cacheable dependencies provided OembedResourceResponse object.
    foreach ($resource_response->getCacheableDependencies() as $dependency) {
      $response->addCacheableDependency($dependency);
    }
    // Merge in cacheable metadata provided by OembedResourceResponse object.
    if ($meta_data = $resource_response->getCacheableMetadata()) {
      $response->getCacheableMetadata()->merge($meta_data);
    }

    return $response;
  }

  /**
   * Gets a URL object from a URL string.
   *
   * @param string $url
   *   The URL of the requested resource.
   *
   * @return mixed
   *   \Drupal\Core\Url if route is determinable; FALSE otherwise.
   */
  protected function getUrlOjbFromUrlString($url) {
    $url = parse_url($url);
    if (!$url) {
      return FALSE;
    }
    $url_object = $this->pathValidator->getUrlIfValid($url['path']);
    if (!$url_object) {
      return FALSE;
    }
    try {
      $url_object->getRouteName();
    }
    catch (\UnexpectedValueException $e) {
      return FALSE;
    }
    return $url_object;
  }

}
