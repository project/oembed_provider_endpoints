<?php

namespace Drupal\oembed_provider_endpoints\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an OembedDataProviderPlugin annotation object.
 *
 * @see \Drupal\oembed_provider_endpoints\OembedDataProviderPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class OembedDataProviderPlugin extends Plugin {

  /**
   * Description of plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
